# React Friends App

This is a simple example Friends List CRUD App

Frontend Stack being used :

- Vite + React 18 + Ts + redux-toolkit + redux-persist+ react-
  bootstrap + formik + yup

## Some short instruction on how to run this project



- After you pulled this repository you should run ``yarn install`` command to install all necessary packages
- As per Vite documentations just try to run ```yarn dev``` command  to run the application on local machine.
- Enjoy playing with this small project user interface.
