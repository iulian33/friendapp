import { useSelector } from "react-redux";
import { ActionButton } from "../../features/ActionButton";
import { useNavigate, useParams } from "react-router";
import { getFriendById } from "../../shared/store/selectors.ts";
import FriendShowCard from "../../widgets/FriendShowCard";

const FriendsDetails = () => {
  const { id } = useParams();
  const selectedFriend = useSelector(getFriendById(id || ""));
  const navigate = useNavigate();

  return (
    <>
      {selectedFriend && (
        <FriendShowCard {...selectedFriend}>
          <>
            <ActionButton
              type="GENERAL"
              label="Back"
              onActionCallback={() => {
                navigate("/");
              }}
            />
            <ActionButton
              type="GENERAL"
              label="Edit"
              spacing="m-lg-3"
              onActionCallback={() => {
                navigate(`/friends/${id}/edit`);
              }}
            />
          </>
        </FriendShowCard>
      )}
    </>
  );
};

export default FriendsDetails;
