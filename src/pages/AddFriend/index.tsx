import { useNavigate } from "react-router";
import { ActionButton } from "../../features/ActionButton";
import { FriendForm } from "../../widgets/FriendForm";

const AddFriend = () => {
  const navigate = useNavigate();

  return (
    <>
      <h1 className="mb-lg-5">Add New Friend</h1>
      <FriendForm formActionLabel="Create Friend" />
      <ActionButton
        type="GENERAL"
        label="Back"
        spacing="mt-3"
        onActionCallback={() => {
          navigate("/");
        }}
      />
    </>
  );
};

export default AddFriend;
