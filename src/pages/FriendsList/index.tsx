import { useNavigate } from "react-router";
import { FriendsTable } from "../../widgets/FriendsTable";
import { ActionButton } from "../../features/ActionButton";

const FriendsList = () => {
  const navigate = useNavigate();

  const onAddNewFriend = () => {
    navigate("/friends/new");
  };

  return (
    <>
      <h1 className="mb-lg-5">Friends List</h1>
      <FriendsTable />
      <ActionButton
        type="ADD"
        label="New Friend"
        spacing="mt-3"
        onActionCallback={onAddNewFriend}
      />
    </>
  );
};

export default FriendsList;
