import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import { getFriendById } from "../../shared/store/selectors.ts";
import { ActionButton } from "../../features/ActionButton";
import { FriendForm } from "../../widgets/FriendForm";

const EditFriend = () => {
  const { id } = useParams();
  const selectedFriend = useSelector(getFriendById(id || ""));
  const navigate = useNavigate();

  return (
    <>
      <h1 className="mb-lg-5">Editing Friend</h1>
      <FriendForm
        selectedFriend={selectedFriend}
        formActionLabel="Update Friend"
      />
      <ActionButton
        type="GENERAL"
        label="Back"
        onActionCallback={() => {
          navigate("/");
        }}
      />
      <ActionButton
        type="GENERAL"
        label="Show"
        spacing="m-lg-3"
        onActionCallback={() => {
          navigate(`/friends/${id}`);
        }}
      />
      <ActionButton type="REMOVE" label="Remove" id={id} />
    </>
  );
};

export default EditFriend;
