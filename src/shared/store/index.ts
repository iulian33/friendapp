import { configureStore } from "@reduxjs/toolkit";
import friendsDataReducer from "./slices/friendsData.ts";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const persistConfig = {
  key: "root",
  storage
};

const persistedFriendsDataReducer = persistReducer(
  persistConfig,
  friendsDataReducer
);

export const store = configureStore({
  reducer: {
    friendsData: persistedFriendsDataReducer
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false
    })
});

export const persistor = persistStore(store);
