import { createSlice } from "@reduxjs/toolkit";
import { FriendsData } from "../../models.ts";
import {
  AddFriendPayload,
  RemoveFriendPayload,
  UpdateFriendPayload
} from "../actionsTypes.ts";

const initialState: FriendsData = {
  friendsList: []
};

const friendsDataSlice = createSlice({
  name: "friendsData",
  initialState: initialState,
  reducers: {
    addFriend: (
      state: FriendsData,
      { payload }: AddFriendPayload
    ) => ({
      ...state,
      friendsList: [...state.friendsList, payload]
    }),

    removeFriend: (
      state: FriendsData,
      { payload }: RemoveFriendPayload
    ) => ({
      ...state,
      friendsList: state.friendsList.filter(
        friend => friend.id !== payload
      )
    }),

    updateFriend: (
      state: FriendsData,
      { payload }: UpdateFriendPayload
    ) => ({
      ...state,
      friendsList: state.friendsList.map(friend =>
        friend.id === payload.id
          ? { ...payload, id: friend.id }
          : friend
      )
    })
  }
});

export const { addFriend, removeFriend, updateFriend } =
  friendsDataSlice.actions;
export default friendsDataSlice.reducer;
