import { Store } from "../models.ts";

export const getFriendById = (routeId: string) => (store: Store) =>
  store.friendsData.friendsList.find(friend => friend.id === routeId);
export const getFriendsList = (store: Store) =>
  store.friendsData.friendsList;
