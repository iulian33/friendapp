import { Friend } from "../models.ts";

export type AddFriendPayload = { payload: Friend };
export type UpdateFriendPayload = { payload: Friend };
export type RemoveFriendPayload = { payload: string };
