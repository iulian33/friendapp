import * as Yup from "yup";

export enum ActionButtonTypes {
  ADD = "dark",
  REMOVE = "danger",
  EDIT = "warning",
  GENERAL = "secondary"
}

export const TableHeadingValues = [
  "Name",
  "Email",
  "Phone",
  "Twitter",
  "Actions"
];

export const FriendFormValidationSchema = Yup.object({
  firstName: Yup.string()
    .max(15, "Must be 15 characters or less")
    .min(3, "Must be minimum 3 characters")
    .matches(/^[A-Za-z]*$/, "Letters are allowed only")
    .required("First Name is required"),
  lastName: Yup.string()
    .max(20, "Must be 20 characters or less")
    .matches(/^[A-Za-z]*$/, "Letters are allowed only")
    .min(3, "Must have min 3 characters")
    .required("Last Name is required"),
  email: Yup.string()
    .email("Invalid email address")
    .required("Required Email Address"),
  phone: Yup.string()
    .required("Phone is required")
    .max(15, "Maximum available digits is 15")
    .matches(/^[0-9+]*$/, "Numbers are allowed only"),
  twitterId: Yup.string()
    .required("Twitter Id is required")
    .max(15, "Must be 15 characters or less")
    .matches(/^@.*$/, "Should start with @ character")
});
