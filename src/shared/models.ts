export interface Store {
  friendsData: FriendsData;
}

export interface FriendsData {
  friendsList: Friend[];
}

export interface Friend {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: number;
  twitterId: string;
}
