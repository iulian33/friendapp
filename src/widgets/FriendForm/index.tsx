import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import {
  // eslint-disable-next-line import/named
  FormikHelpers,
  Form as FormikForm,
  Formik
} from "formik";
import {
  addFriend,
  updateFriend
} from "../../shared/store/slices/friendsData.ts";
import { FriendFormValidationSchema } from "../../shared/constants.ts";
import { ActionButton } from "../../features/ActionButton";
import { Friend } from "../../shared/models.ts";
import { FormField } from "../../features/FormField";

type FriendFormProps = {
  formActionLabel: string;
  selectedFriend?: Friend;
};

export const FriendForm = ({
  selectedFriend = {} as Friend,
  formActionLabel
}: FriendFormProps) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onFormSubmitted = (
    values: Friend,
    { setSubmitting }: FormikHelpers<Friend>
  ) => {
    dispatch(
      selectedFriend?.id
        ? updateFriend(values)
        : addFriend({ ...values, id: crypto.randomUUID() })
    );
    navigate(`/friends`);
    setSubmitting(false);
  };

  return (
    <Formik
      initialValues={selectedFriend}
      onSubmit={onFormSubmitted}
      validationSchema={FriendFormValidationSchema}
    >
      {({
        values,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
      }) => (
        <FormikForm>
          <FormField
            label="First Name"
            type="text"
            name="firstName"
            value={values.firstName}
            placeholder="First Name"
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <FormField
            label="Last Name"
            type="text"
            name="lastName"
            value={values.lastName}
            placeholder="Last Name"
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <FormField
            label="Email"
            type="email"
            name="email"
            value={values.email}
            placeholder="name@example.com"
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <FormField
            label="Phone"
            type="text"
            name="phone"
            value={values.phone}
            placeholder="Phone"
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <FormField
            label="Twitter"
            type="text"
            name="twitterId"
            value={values.twitterId}
            placeholder="Twitter"
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <div>
            <ActionButton
              disabled={isSubmitting}
              type="ADD"
              label={formActionLabel}
              onActionCallback={handleSubmit}
            />
            <button type="submit" hidden />
          </div>
        </FormikForm>
      )}
    </Formik>
  );
};
