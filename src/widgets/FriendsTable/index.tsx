import Table from "react-bootstrap/Table";
import { useSelector } from "react-redux";
import { TableHeadingValues } from "../../shared/constants.ts";
import { getFriendsList } from "../../shared/store/selectors.ts";
import { EmptyFriendsList } from "../EmptyFriendsList";
import { FriendRow } from "../../features/FriendRow";

export const FriendsTable = () => {
  const friendsList = useSelector(getFriendsList);

  return (
    <>
      {friendsList.length ? (
        <Table striped bordered hover responsive size="xxl">
          <thead className="heading-table">
            <tr className="table-dark">
              {TableHeadingValues.map((heading, idx) => (
                <th
                  key={idx}
                  className={
                    heading === "Actions" ? "text-lg-center" : ""
                  }
                >
                  {heading}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {friendsList.map(friend => (
              <FriendRow key={friend.id} {...friend} />
            ))}
          </tbody>
        </Table>
      ) : (
        <EmptyFriendsList />
      )}
    </>
  );
};
