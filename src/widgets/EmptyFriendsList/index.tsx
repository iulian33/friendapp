import Card from "react-bootstrap/Card";

export const EmptyFriendsList = () => {
  return (
    <Card>
      <Card.Header>No friends !!</Card.Header>
      <Card.Body>
        <blockquote className="blockquote p-lg-4">
          <p>Sad for you, but you don&apos;t have any friends !!</p>
          <footer className="blockquote-footer">
            As suggestion --
            <cite title="use button to allow magic happen">
              Try use the button below to see the magic
            </cite>
          </footer>
        </blockquote>
      </Card.Body>
    </Card>
  );
};
