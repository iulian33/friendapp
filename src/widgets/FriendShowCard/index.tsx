import Card from "react-bootstrap/Card";
import { Friend } from "../../shared/models.ts";

type FriendShowCardProps = {
  children?: JSX.Element;
} & Friend;

const FriendShowCard = ({
  firstName,
  lastName,
  twitterId,
  email,
  phone,
  children
}: FriendShowCardProps) => {
  return (
    <Card className="text-bg-light border-light p-lg-4">
      <Card.Body>
        <Card.Title>
          <h1 className="mb-lg-3">
            {firstName} {lastName}
          </h1>
        </Card.Title>
        <Card.Text>
          {email} | {phone} | <strong>{twitterId}</strong>
        </Card.Text>
        {children}
      </Card.Body>
    </Card>
  );
};

export default FriendShowCard;
