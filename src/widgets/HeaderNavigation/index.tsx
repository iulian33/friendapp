import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { Link } from "react-router-dom";

export const HeaderNavigation = () => {
  return (
    <Navbar
      fixed="top"
      expand="lg"
      className="bg-dark header-navigation"
      variant="dark"
    >
      <Container>
        <Link to="/friends" relative="path" className="no-underline">
          <Navbar.Brand>Friend App</Navbar.Brand>
        </Link>
        <Navbar.Collapse
          id="basic-navbar-nav"
          className="flex-wrap justify-content-end"
        >
          <Nav>
            <Link to="/friends/new" className="nav-link">
              Add Friend
            </Link>
            <Nav.Link href="#about">About</Nav.Link>
            <Nav.Link href="#editProfile">Edit Profile</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};
