import TableActions from "../TableActions";
import { Friend } from "../../shared/models.ts";
import { Link } from "react-router-dom";

export const FriendRow = ({
  id,
  firstName,
  lastName,
  email,
  phone,
  twitterId
}: Friend) => {
  return (
    <tr>
      <td>
        <Link to={`/friends/${id}`} className="no-underline">
          {firstName} {lastName}
        </Link>
      </td>
      <td>{email}</td>
      <td>{phone}</td>
      <td>
        <strong>{twitterId}</strong>
      </td>
      <td className="text-lg-center">
        <TableActions id={id} />
      </td>
    </tr>
  );
};
