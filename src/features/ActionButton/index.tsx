import { Button } from "react-bootstrap";
import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import { ActionButtonTypes } from "../../shared/constants.ts";
import { removeFriend } from "../../shared/store/slices/friendsData.ts";

type CallbackAction = () => void;

type ActionButtonProps = {
  type: "ADD" | "REMOVE" | "EDIT" | "GENERAL";
  label: string;
  id?: string;
  spacing?: string;
  disabled?: boolean;
  onActionCallback?: CallbackAction | null;
};

export const ActionButton = ({
  type,
  label,
  id = "",
  spacing,
  disabled,
  onActionCallback = null
}: ActionButtonProps) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleRemoveFriend = () => {
    dispatch(removeFriend(id));
    navigate(`/friends`);
  };

  return (
    <Button
      disabled={disabled}
      variant={ActionButtonTypes[type]}
      className={spacing}
      onClick={
        onActionCallback ? onActionCallback : handleRemoveFriend
      }
    >
      {label}
    </Button>
  );
};
