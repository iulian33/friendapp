import { ErrorMessage } from "formik";
import Form from "react-bootstrap/Form";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import * as React from "react";

type FormFieldProps = {
  label: string;
  type: string;
  name: string;
  value: string | number;
  placeholder: string;
  onChange: React.ChangeEventHandler;
  onBlur: React.ChangeEventHandler;
};

export const FormField = ({
  name,
  type,
  label,
  value,
  onBlur,
  onChange,
  placeholder
}: FormFieldProps) => {
  return (
    <FloatingLabel
      controlId="floatingInput"
      label={label}
      className="mb-3"
    >
      <Form.Control
        type={type}
        name={name}
        value={value}
        onBlur={onBlur}
        onChange={onChange}
        placeholder={placeholder}
      />
      <ErrorMessage
        name={name}
        component="div"
        className="invalid-feedback"
      />
    </FloatingLabel>
  );
};
