import { ButtonGroup } from "react-bootstrap";
import { ActionButton } from "../ActionButton";
import { useNavigate } from "react-router";

type TableActionsProps = {
  id: string;
};

const TableActions = ({ id }: TableActionsProps) => {
  const navigate = useNavigate();

  const onEditHandler = () => {
    navigate(`/friends/${id}/edit`);
  };

  return (
    <ButtonGroup size="sm">
      <ActionButton
        type="EDIT"
        label="Edit"
        onActionCallback={onEditHandler}
      />
      <ActionButton type="REMOVE" label="Remove" id={id} />
    </ButtonGroup>
  );
};

export default TableActions;
