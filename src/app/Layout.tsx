import React from "react";
import Container from "react-bootstrap/Container";
import { store } from "../shared/store";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { HeaderNavigation } from "../widgets/HeaderNavigation";

type LayoutWrap = {
  children: React.JSX.Element | React.JSX.Element[];
};

export const Layout = ({ children }: LayoutWrap) => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <HeaderNavigation />
        <Container>{children}</Container>
      </BrowserRouter>
    </Provider>
  );
};
