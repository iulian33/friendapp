import { Layout } from "./Layout.tsx";
import { Navigate, Route, Routes } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import FriendsList from "../pages/FriendsList";
import AddFriend from "../pages/AddFriend";
import FriendDetails from "../pages/FriendDetails";
import EditFriend from "../pages/EditFriend";
import { persistor } from "../shared/store";

function App() {
  return (
    <Layout>
      <PersistGate loading={null} persistor={persistor}>
        <Routes>
          <Route
            path="/"
            element={<Navigate to="/friends" replace={true} />}
          />
          <Route path="/friends" element={<FriendsList />} />
          <Route path="/friends/new" element={<AddFriend />} />
          <Route path="/friends/:id" element={<FriendDetails />} />
          <Route path="/friends/:id/edit" element={<EditFriend />} />
        </Routes>
      </PersistGate>
    </Layout>
  );
}

export default App;
